// $Id: hashset.c,v 1.10 2015-03-13 13:04:25-07 - - $

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "hashset.h"
#include "strhash.h"

#define HASH_NEW_SIZE 15

typedef struct hashnode hashnode;
typedef size_t hashset_t;
struct hashnode {
   char *word;
   hashnode *link;
};

struct hashset {
   size_t size;
   size_t load;
   hashnode **chains;
};

hashset *new_hashset (void)
{
   hashset *this = malloc (sizeof (struct hashset));
   assert (this != NULL);
   this->size = HASH_NEW_SIZE;
   this->load = 0;
   size_t sizeof_chains = this->size * sizeof (hashnode *);
   this->chains = malloc (sizeof_chains);
   assert (this->chains != NULL);
   memset (this->chains, 0, sizeof_chains);
   return this;
}

void increase_hash(hashset *this)
{
size_t size = this->size*2 + 1;
hashnode **new_chain = calloc(size, sizeof(hashnode *));
for(size_t i = 0; i < this->size; i++){
hashnode *temp = this->chains[i];
while(temp != NULL){
//co search the hashset
hashset_t hash_index = strhash(temp->word) % size;
hashnode *link = temp->link;
//linked list
temp->link = new_chain[hash_index];
new_chain[hash_index] = temp;
temp = link;
  }
 }
 //free the chain
 free(this->chains);
 this->chains = new_chain;
 this->size = size;
 }

void free_hashset (hashset *this)
{
    for(size_t i = 0; i < this->size; i++)
    {
        hashnode *temp = this->chains[i];
        while(temp != NULL)
        {
            hashnode *link = temp->link;
            free(temp->word);
            free(temp);
            temp = link;
        }
    }
    free(this->chains);
    free(this);
}

void put_hashset (hashset *this, const char *item)
{
//increase the number of words in the hashset
this->load++;
if(this->load * 2 >this->size){
increase_hash(this);
}
hashset_t hash_index = strhash (item) % this->size;
hashnode *curr = this->chains[hash_index];

for(; curr !=NULL; curr = curr->link)
{
//If word is in the hashset
if(strcmp(curr->word, item) == 0) return;
}
//Create chain
hashnode *node = malloc(sizeof(struct hashnode));
node->link = this->chains[hash_index];
//copy string item into word
node->word = strdup(item);
this->chains[hash_index] = node;
}

bool has_hashset (hashset *this, const char *item)
{
 hashset_t hash_index = strhash (item) % this->size;
 hashnode *curr = this->chains[hash_index];
 for(; curr != NULL; curr = curr->link)
 {
 if(strcmp(curr->word, item) == 0)
 return true;
 }
  return false;
}

void x_print (hashset *this)
{
    printf("%d words in the hash set\n", (int)this->load);
    printf("%d size of the hash array\n", (int) this->size);
    // used to keep track of count of different chains by length
    int *chaincounter = calloc(100, sizeof(int));
    for(size_t i = 0; i < this->size; i++){
        hashnode *head = this->chains[i];
        int chain_index = 0;
        // length of chain
        for(; head != NULL; head = head->link) chain_index++;
        chaincounter[chain_index]++;
    }
    // prints out info
    for(int index = 1; index < 100; index++){
        if(chaincounter[index] != 0){
            printf("%5d chains of size %d\n", chaincounter[index], index);
        }
    }
    free(chaincounter);
}

void hash_print(hashset *this){
    for(size_t i = 0; i < this->size; i++){
        hashnode *temp = this->chains[i];
        // print head of chain
        if(temp != NULL){
            hashset_t code = strhash(temp->word);
            printf("array[%10d] = %12u \"%s\"\n",
                   (int)i, (int)code, temp->word);
            temp = temp->link;
        }
        // print the chain
        while(temp != NULL){
            hashset_t code = strhash(temp->word);
            printf("%18s= %12u \"%s\"\n", "" , (int)code, temp->word);
            temp = temp->link;
        }
    }
}

